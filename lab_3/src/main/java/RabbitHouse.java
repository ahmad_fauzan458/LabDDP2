import java.util.Scanner;
/**
 * Code Author (Mahasiswa):
 * @author Ahmad Fauzan Amirul Isnain, NPM 1706979152, Kelas DDP-2 D, GitLab Account: ahmad_fauzan458
 */
public class RabbitHouse {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
		System.out.print("Masukkan input: ");
        String[] perintah = input.nextLine().toUpperCase().split(" ");
        try {
            switch (perintah[0]) {
                case "NORMAL":
                    System.out.println(kelinci(perintah[1].length()));
                    break;
                case "PALINDROM":
                    System.out.println(kelinci(perintah[1].length()) -
                            kelinciMati(perintah[1].toUpperCase()));
                    break;
                default:
                    System.out.println("Warning : Input tidak sesuai");
            }
        }
        catch (Exception IndexOutOfBoundsException){
            System.out.println("Warning : Input tidak sesuai");
        }

    }

    /**
     * Static method untuk menghitung jumlah kelinci dalam satu pohon keturunan
     * @param jumlahHurufKelinci Jumlah huruf pada nama kelinci yang ingin dicek
     * @return Jumlah kelinci dalam satu pohon keturunan
     */
    public static int kelinci(int jumlahHurufKelinci){
        if (jumlahHurufKelinci<=1) return 1;
        else return 1 + jumlahHurufKelinci* kelinci(jumlahHurufKelinci-1);
    }

    /**
     * Static method untuk menghitung jumlah kelinci yang mati dalam satu pohon keturunan
     * Kelinci yang mati adalah kelinci palindrom dan keturunannya
     * @param namaKelinci Nama kelinci yang ingin dicek
     * @return Jumlah kelinci yang mati
     */
    public static int kelinciMati(String namaKelinci){
        if (namaKelinci.length()<=1) return 1;

        if (namaKelinci.charAt(0)==namaKelinci.charAt(namaKelinci.length()-1)){
            return kelinci(namaKelinci.length());
        }
        else {
            int jumlahMati = 0;
            for (int i=0; i<namaKelinci.length(); i++){
                String anak = namaKelinci.substring(0, i) + namaKelinci.substring(i+1, namaKelinci.length());
                jumlahMati += kelinciMati(anak);
            }
            return jumlahMati;
        }
    }
}
