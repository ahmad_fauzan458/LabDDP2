/**
 * Code Author (Mahasiswa):
 * @author Ahmad Fauzan Amirul Isnain, NPM 1706979152, Kelas DDP-2 D, GitLab Account: ahmad_fauzan458
 */
class Manusia{
    private String nama;
    private int umur;
    private int uang;
    private float kebahagiaan = 50;
    private boolean sudahTiada = false;
    private static Manusia manusiaTerakhir;

    /**
     * Constructor untuk instansiasi objek manusia dengan nama, umur, dan uang
     * @param nama merupakan nama manusia
     * @param umur merupakan umur manusia
     * @param uang merupakan uang manusia
     */
    public Manusia(String nama, int umur, int uang){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        manusiaTerakhir = this;
    }

    /**
     * Constructor untuk instansiasi objek manusia dengan nama dan umur
     * @param nama merupakan nama manusia
     * @param umur merupakan umur manusia
     */
    public Manusia(String nama, int umur){
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        manusiaTerakhir = this;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public float getKebahagiaan() {
        return kebahagiaan;
    }

    public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

    public boolean isSudahTiada() {
        return sudahTiada;
    }

    public void setSudahTiada(boolean sudahTiada) {
        this.sudahTiada = sudahTiada;
    }

    public static Manusia getManusiaTerakhir() {
        return manusiaTerakhir;
    }

    public static void setManusiaTerakhir(Manusia manusiaTerakhir) {
        Manusia.manusiaTerakhir = manusiaTerakhir;
    }

    /**
     * mengecek apakah kebahagiaan baru kurang dari batas minimum kebahagiaan atau tidak
     * @param kebahagiaanBaru yang akan di cek
     * @return kebahagiaan baru jika tidak kurang dari batas minimum, 0.0f jika melewati batas minimum
     */
    public static float cekMinimumKebahagiaan(float kebahagiaanBaru){
        if (kebahagiaanBaru<0.0) kebahagiaanBaru = 0.0f;
        return kebahagiaanBaru;
    }

    /**
     * mengecek apakah kebahagiaan baru lebih dari batas maksimum kebahagiaan atau tidak
     * @param kebahagiaanBaru yang akan di cek
     * @return kebahagiaan baru jika tidak lebih dari batas maksimum, 100.0f jika melewati batas minimum
     */
    public static float cekMaksimumKebahagiaan(float kebahagiaanBaru){
        if (kebahagiaanBaru>100.0f) kebahagiaanBaru = 100.0f;
        return kebahagiaanBaru;
    }

    /**
     * mengkalkulasi jumlah uang yang akan diberikan, kemudian menjalankan method beri uang menggunakan
     * parameter penerima dan jumlah, jumlah sesuai dengan jumlah uang yang telah dikalkulasi
     * @param penerima manusia yang akan menerima uang
     */
    public void beriUang(Manusia penerima){
        int jumlah = 0;
        for (int i=0; i<penerima.nama.length(); i++){
            jumlah += (int)(penerima.nama.charAt(i));
        }
        jumlah *= 100;

        this.beriUang(penerima, jumlah);
    }

    /**
     * memberikan uang kepada [penerima] sebanyak [jumlah], jika uang yang dimiliki pemberi tersebut cukup,
     * maka kebahagiaan mereka berdua (pemberi dan penerima) akan bertambah
     * jika pemberi atau penerima sudah tiada maka tidak dapat memberi uang
     * @param penerima manusia yang akan menerima uang
     * @param jumlah jumlah uang yang akan diberikan
     */
    public void beriUang(Manusia penerima, int jumlah){
        if (this.sudahTiada) {
            System.out.printf("%s telah tiada\n", this.nama);
            return;
        }

        if (penerima.sudahTiada){
            System.out.printf("%s telah tiada\n", penerima.nama);
            return;
        }

        if (this.uang>=jumlah) {
            float kebahagiaanBaruPemberi = this.kebahagiaan + jumlah/6000.0f;
            float kebahagiaanBaruPenerima = penerima.kebahagiaan + jumlah/6000.0f;
            this.kebahagiaan = cekMaksimumKebahagiaan(kebahagiaanBaruPemberi);
            penerima.kebahagiaan = cekMaksimumKebahagiaan(kebahagiaanBaruPenerima);
            this.uang -= jumlah;
            penerima.uang+= jumlah;
            System.out.printf("%s memberi uang sebanyak %d kepada %s, " +
                    "mereka berdua senang :D\n", this.nama, jumlah, penerima.nama);
        }
        else System.out.printf("%s ingin memberi uang kepada %s " +
                "namun tidak memiliki cukup uang :'(\n", this.nama, penerima.nama);
    }

    /**
     * method untuk seorang manusia bekerja, mendapatkan uang namun kehilangan kebahagiaan
     * jika umur manusia tersebut masih dibawah usia kerja minimum (18) maka tidak dapat bekerja
     * jika manusia tersebut sudah tiada maka tidak dapat bekerja
     * jika bebanKerjaTotal lebih dari kebahagiaan manusia terssebut, maka manusia bekerja half time
     * @param durasi
     * @param bebanKerja
     */
    public void bekerja(int durasi, int bebanKerja){
        if (this.sudahTiada) {
            System.out.printf("%s telah tiada\n", this.nama);
            return;
        }

        if (this.umur<18) {
            System.out.printf("%s belum boleh bekerja karena " +
                    "masih dibawah umur D:\n", this.nama);
            return;
        }

        boolean fullTime = true;
        int bebanKerjaTotal = durasi * bebanKerja;

        if (bebanKerjaTotal>this.kebahagiaan){
            durasi = (int)(this.kebahagiaan/bebanKerja);
            bebanKerjaTotal = durasi * bebanKerja;
            fullTime = false;
        }
        float kebahagiaanBaru = this.kebahagiaan - bebanKerjaTotal;
        this.kebahagiaan = cekMinimumKebahagiaan(kebahagiaanBaru);

        int pendapatan = bebanKerjaTotal*10000;
        this.uang += pendapatan;

        if(fullTime) System.out.printf("%s bekerja full time, total pendapatan : %d\n", this.nama, pendapatan);
        else System.out.printf("%s tidak bekerja secara full time karena sudah terlalu lelah, " +
                "total pendapatan : %d\n", this.nama, pendapatan);
    }

    /**
     * Method untuk seorang manusia berekreasi, mendapatkan kebahagiaan dengan mengorbankan uang
     * Biaya rekreasi/uang yang harus dikeluarkan untuk berekreasi di tempat [namaTempat] dikalkulasi menggunakan rumus
     * panjang nama tempat * 10000
     * jika uang manusia tersebut tidak cukup maka tidak dapat berekreasi
     * jika manusia tersebut sudah tiada maka tidak dapat berekreasi
     * @param namaTempat nama tempat rekreasi
     */
    public void rekreasi(String namaTempat){
        if (this.sudahTiada) {
            System.out.printf("%s telah tiada\n", this.nama);
            return;
        }

        int biaya = namaTempat.length()*10000;

        if (this.uang >= biaya){
            this.uang-=biaya;
            float kebahagiaanBaru = this.kebahagiaan + namaTempat.length();
            this.kebahagiaan = cekMaksimumKebahagiaan(kebahagiaanBaru);
            System.out.printf("%s berekreasi di %s, %s senang :)\n",
                    this.nama, namaTempat, this.nama);
        }
        else System.out.printf("%s tidak mempunyai cukup uang untuk berekreasi di %s :(\n",
                this.nama, namaTempat);

    }

    /**
     * method yang dipanggil jika seorang manusia sakit, kehilangan kebahagiaan
     * jika manusia tersebut sudah tiada maka tidak dapat sakit
     * @param namaPenyakit nama penyakit yang diderita manusia tersebut
     */
    public void sakit(String namaPenyakit){
        if (this.sudahTiada) {
            System.out.printf("%s telah tiada\n", this.nama);
            return;
        }

        float kebahagiaanBaru = this.kebahagiaan - namaPenyakit.length();
        this.kebahagiaan = cekMinimumKebahagiaan(kebahagiaanBaru);
        System.out.printf("%s terkena penyakit %s :O\n", this.nama, namaPenyakit);

    }

    /**
     * method yang dipanggil jika seorang manusia meninggal
     * Kemudian semua sisa uang manusia tersebut disumbangkan ke "Manusia Terakhir"
     * (yang disebut "Manusia Terakhir" adalah Object Manusia yang diinstansiasi terakhir kali).
     * jika Manusia yang meninggal adalah "Manusia Terakhir", maka uang tersebut akan hangus
     * orang yang sudah meninggal maka tidak dapat melakukan behavior apapun dan juga tidak bisa diberi uang lagi
     * jika manusia tersebut sudah tiada maka tidak dapat meninggal lagi
     */
    public void meninggal(){
        if (this.sudahTiada) {
            System.out.printf("%s telah tiada\n", this.nama);
            return;
        }

        this.sudahTiada = true;
        System.out.printf("%s meninggal dengan tenang, kebahagiaan : %s\n",
                this.nama, this.kebahagiaan);
        if (this!=manusiaTerakhir){
            manusiaTerakhir.uang += this.uang;
            System.out.printf("Semua harta %s disumbangkan untuk %s\n", this.nama, manusiaTerakhir.nama );
        }
        else {
            System.out.printf("Semua harta %s hangus\n", this.nama);
        }
        this.uang = 0;
    }

    /**
     * method untuk menyusun identitas seorang manusia
     * @return informasi mengenai manusia tersebut berupa nama, umur, uang dan kebahagiaan
     * jika sudah meninggal maka terdapat Almarhum didepan nama
     */
    public String toString(){
        String nama = this.nama;
        if (this.sudahTiada) nama = "Almarhum " + nama;
        return String.format("Nama\t\t: %s\nUmur\t\t: %d\nUang\t\t: %d\nKebahagiaan\t: %s",
                nama, this.umur, this.uang, this.kebahagiaan);
    }
}
