package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Class representing a user, willing to attend event(s)
 *
 * @author Ahmad Fauzan A.I. 1706979152 DDP2-D
 */
public class User {
    /**
     * Name of user
     */
    private String name;

    /**
     * List of events this user plans to attend
     */
    private ArrayList<Event> events;

    /**
     * Constructor
     * Initializes a user object with given name and empty event list
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     *
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Method that count the total cost of events that user registered.
     *
     * @return the total cost.
     */
    public BigInteger getTotalCost() {
        BigInteger totalCost = BigInteger.ZERO;
        for (Event event : events) {
            totalCost = totalCost.add(event.getCost());
        }
        return totalCost;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping
     * with currently planned events.
     *
     * @return true if the event if successfully added, false otherwise
     */
    public boolean addEvent(Event newEvent) {
        for (Event event : events) {
            if (Event.isOverlap(event, newEvent)) {
                return false;
            }
        }
        events.add(newEvent);
        return true;
    }

    /**
     * Returns the list of events this user plans to attend,
     * Sorted by their starting time.
     * Note: The list returned from this method is a copy of the actual
     * events field, to avoid mutation from external sources
     *
     * @return list of events this user plans to attend
     */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> events = new ArrayList<>(this.events);
        Event.sortEvent(events);
        return events;
    }
}
