package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;

/**
 * Class representing event managing system
 *
 * @author Ahmad Fauzan A.I. 1706979152 DDP2-D
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method to add an event to system.
     *
     * @param name           name of the event
     * @param startTimeStr   start time of the event
     * @param endTimeStr     end time of the event
     * @param costPerHourStr cost per hour of the event
     * @return information the resullt of adding the event.
     */
    public String addEvent(String name, String startTimeStr,
                           String endTimeStr, String costPerHourStr) {
        String info;
        if (isEventRegistered(name)) {
            info = String.format("Event %s sudah ada!", name);
        } else {
            Event event = new Event(name, startTimeStr, endTimeStr, costPerHourStr);
            if (Event.isValidTime(event.getStartTime(), event.getEndTime())) {
                events.add(event);
                info = String.format("Event %s berhasil ditambahkan!", name);
            } else {
                info = "Waktu yang diinputkan tidak valid!";
            }

        }
        return info;
    }

    /**
     * Method to check registered event.
     *
     * @param name name of the event
     * @return true if the event is already registered
     */
    private boolean isEventRegistered(String name) {
        return !(this.findEvent(name) == null);
    }

    /**
     * Method to find an event with the name.
     *
     * @param name name of the event
     * @return the event if found, null if not found
     */
    private Event findEvent(String name) {
        for (Event event : events) {
            if (event.getName().equals(name)) {
                return event;
            }
        }
        return null;
    }

    /**
     * Method to add user to system.
     *
     * @param name name of the event
     * @return true if the event successfully added.
     */
    public String addUser(String name) {
        String info;
        if (isUserRegistered(name)) {
            info = String.format("User %s sudah ada!", name);
        } else {
            users.add(new User(name));
            info = String.format("User %s berhasil ditambahkan!", name);
        }
        return info;
    }

    /**
     * Method to check registered user.
     *
     * @param name name of the user
     * @return true if the user already registered
     */
    private boolean isUserRegistered(String name) {
        return !(findUser(name) == null);
    }

    /**
     * Method to find user using the name.
     *
     * @param name name of the user
     * @return the user if found, null if not found
     */
    private User findUser(String name) {
        for (User user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Method getter for user with spesific name.
     *
     * @param name name of the user
     * @return the user or null if the user not exist
     */
    public User getUser(String name) {
        return findUser(name);
    }

    /**
     * Method getter for event with spesific name.
     *
     * @param name name of the user
     * @return the event or null if the event not exist
     */
    public Event getEvent(String name) {
        return findEvent(name);
    }

    /**
     * Method to register an user to an event.
     *
     * @param userName  name of the user
     * @param eventName name of the event
     * @return information about the register result
     */
    public String registerToEvent(String userName, String eventName) {
        String info;
        User user = findUser(userName);
        Event event = findEvent(eventName);
        if (user == null && event == null) {
            info = String.format("Tidak ada pengguna dengan nama %s"
                    + " dan acara dengan nama %s!", userName, eventName);
        } else if (event == null) {
            info = String.format("Tidak ada acara dengan nama %s!", eventName);
        } else if (user == null) {
            info = String.format("Tidak ada pengguna dengan nama %s!", userName);
        } else if (user.addEvent(event)) {
            info = String.format("%s berencana menghadiri %s!", userName, eventName);
        } else {
            info = String.format("%s sibuk sehingga tidak dapat menghadiri %s!", userName, eventName);
        }
        return info;
    }
}