package lab9.event;

import java.time.LocalDateTime;
import java.math.BigInteger;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class representing an event and its properties
 *
 * @author Ahmad Fauzan A.I. 1706979152 DDP2-D
 */
public class Event {
    private String name;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private BigInteger cost;
    private DateTimeFormatter inFormatter
            = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
    private DateTimeFormatter outFormatter
            = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");

    /**
     * Constructor for Event.
     *
     * @param name        name of the event
     * @param startTime   start time of the event
     * @param endTime     end time of the event
     * @param cost cost of the event
     */
    public Event(String name, String startTime,
                 String endTime, String cost) {
        this.name = name;
        this.startTime = LocalDateTime.parse(startTime, inFormatter);
        this.endTime = LocalDateTime.parse(endTime, inFormatter);
        this.cost = new BigInteger(cost);
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for startTime field.
     *
     * @return start time of this event instance
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Accessor for endTime field.
     *
     * @return end time of this event instance
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Accessor for cost field.
     *
     * @return cost per hour of this event instance
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Method that describe string representation of the event,
     * include name, start time, end time, and cost.
     *
     * @return string representation of the event
     */
    public String toString() {
        return String.format("%s\nWaktu mulai: %s\nWaktu selesai: " +
                        "%s\nBiaya kehadiran: %s", name, startTime.format(outFormatter),
                endTime.format(outFormatter), cost);
    }

    /**
     * Compare start time and end time of an event,
     * the time is valid when the start time is before the end time.
     *
     * @param startTime start time of an event
     * @param endTime   end time of an event
     * @return true if the time is valid
     */
    public static boolean isValidTime(LocalDateTime startTime, LocalDateTime endTime) {
        return startTime.isBefore(endTime);
    }

    /**
     * Method for check overlapping event.
     *
     * @param event1 first event that want to be checked
     * @param event2 second event that want to be checked
     * @return true if event1 and event2 are overlapping.
     */
    public static boolean isOverlap(Event event1, Event event2) {
        return (event2.getStartTime().isBefore(event1.getEndTime())
                && event2.getEndTime().isAfter(event1.getStartTime()));
    }

    /**
     * Method for sorting ArrayList of event.
     *
     * @param events events that want to be sorted.
     */
    public static void sortEvent(ArrayList<Event> events) {
        events.sort(Comparator.comparing(Event::getStartTime));
    }
}
