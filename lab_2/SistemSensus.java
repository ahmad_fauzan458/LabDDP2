import javax.xml.bind.SchemaOutputResolver;
import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Ahmad Fauzan Amirul Isnain, NPM 1706979152, Kelas DDP-2 D, GitLab Account: ahmad_fauzan458
 */

public class SistemSensus {

    public static void warningMessage(){
        System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            System.exit(0);
    }

    public static void main(String[] args) {
        // Buat input scanner baru
        Scanner input = new Scanner(System.in);
        short panjang=0, lebar=0, tinggi=0;
        float berat= 0.0f;
        byte makanan = 0, jumlahCetakan = 0;
        String tanggalLahir="", catatan ="";

        // bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // User Interface untuk meminta masukan
        System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
                "--------------------\n" +
                "Nama Kepala Keluarga   : ");
        String nama = input.nextLine();
        System.out.print("Alamat Rumah           : ");
        String alamat = input.nextLine();


        try {
            System.out.print("Panjang Tubuh (cm)     : ");
            panjang = Short.parseShort(input.nextLine());
            if (panjang <= 0 || panjang > 250) warningMessage();

            System.out.print("Lebar Tubuh (cm)       : ");
            lebar = Short.parseShort(input.nextLine());
            if (lebar <= 0 || lebar > 250) warningMessage();

            System.out.print("Tinggi Tubuh (cm)      : ");
            tinggi = Short.parseShort(input.nextLine());
            if (tinggi <= 0 || tinggi > 250) warningMessage();

            System.out.print("Berat Tubuh (kg)       : ");
            berat = Float.parseFloat(input.nextLine());
            if (berat <= 0 && berat > 150) warningMessage();

            System.out.print("Jumlah Anggota Keluarga: ");
            makanan = Byte.parseByte(input.nextLine());
            if (makanan <= 0 && berat > 20) warningMessage();

            System.out.print("Tanggal Lahir          : ");
            tanggalLahir = input.nextLine();

            System.out.print("Catatan Tambahan       : ");
            catatan = input.nextLine();

            System.out.print("Jumlah Cetakan Data    : ");
            jumlahCetakan = Byte.parseByte(input.nextLine());
        }
        catch (Exception e){
            warningMessage();
        }

        //bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        //menghitung rasio berat per volume
        int rasio = (int) ((berat) / ((panjang*lebar*tinggi)*0.000001));
        System.out.println();
        for (byte nomor=1; nomor<=jumlahCetakan; nomor++) {
            // meminta masukan terkait nama penerima hasil cetak data
            System.out.print("Pencetakan " + nomor + " dari " + jumlahCetakan + " untuk: ");
            String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
            System.out.println("DATA SIAP DICETAK UNTUK " + penerima);
            System.out.println("-----------------");

            // mencetak hasil
            String hasil = String.format("%s - %s\nLahir pada tanggal %s\nRasio Berat Per Volume     = %d kg/m^3",
                    nama, alamat, tanggalLahir, rasio);
            System.out.println(hasil);

            //mencetak catatan tambahan jika ada, jika tidak mencetak "Tidak ada catatan tambahan"
            if (catatan.equals("")) System.out.println("Tidak ada catatan tambahan\n");
            else System.out.println("Catatan: " + catatan +"\n");
        }


        // Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
        // menghitung nomor keluarga dari parameter yang telah disediakan
        int jumlahAscii=0;
        for (int i=0; i<nama.length(); i++){
            jumlahAscii += (int)nama.charAt(i);
        }

        // menggabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
        String nomorKeluarga = nama.charAt(0) +
                String.valueOf(((panjang * tinggi * lebar) + jumlahAscii) % 10000);

		// menghitung anggaran makanan per tahun
		int anggaran = 50000*365*makanan;

		// Menghitung umur dari tanggalLahir
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]);
        short umur = (short)(2018 - tahunLahir);

		// proses menentukan apartemen sesuai kriteria soal

        String namaApartemen;
        String kabupaten;
        if (umur>=0 && umur<=18) {
            namaApartemen = "PPMT";
            kabupaten = "Rotunda";
        }
        else {
            if (anggaran>=0 && anggaran<=100000000) {
                namaApartemen = "Teksas";
                kabupaten = "Sastra";
            }
            else{
                namaApartemen = "Mares";
                kabupaten = "Margonda";
            }
        }


		// mencetak rekomendasi
		String rekomendasi = "REKOMENDASI APARTEMEN\n" +
                "--------------------\n" +
                String.format("MENGETAHUI: Identitas keluarga: %s - %s\n", nama, nomorKeluarga) +
                String.format("MENIMBANG:  Anggaran makanan tahunan: Rp %d\n", anggaran) +
                String.format("            Umur kepala keluarga: %d tahun\n", umur) +
                String.format("MEMUTUSKAN: keluarga %s akan ditempatkan di:\n", nama) +
                String.format("%s, kabupaten %s", namaApartemen, kabupaten);
        System.out.println(rekomendasi);
        input.close();
    }
}