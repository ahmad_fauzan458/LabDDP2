package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Fauzan A.I.
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     *
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     * @throws InvalidCharacterException if the Kiss Key string  contains character
     *                                   other than A-Z, a-z, and @
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException, InvalidCharacterException{
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws SizeTooBigException  if the message exceeding 10 Kbit.
     */
    public XoxoMessage encrypt (String message) throws SizeTooBigException {
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     *
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if seed is out of inclusive range 0 - 36
     * @throws SizeTooBigException  if the message exceeding 10 Kbit.
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException, SizeTooBigException {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE ) {
            throw new RangeExceededException("Seed is out of inclusive range 0-36");
        }
        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     *
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException  if the message exceeding 10 Kbit.
     */
    private String encryptMessage(String message) throws SizeTooBigException {

        if (message.getBytes().length > 1250){
            //1250 bytes = 10 Kbit
            throw new SizeTooBigException("Message exceeded 10 Kbit");
        }

        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

