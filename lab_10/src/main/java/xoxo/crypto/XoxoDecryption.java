package xoxo.crypto;

import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Fauzan A.I.
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     * @param hugKeyString the string from hug key
     * @throws KeyTooLongException if key string exceeding 28 in length
     */
    public XoxoDecryption(String hugKeyString) throws KeyTooLongException {
        if(hugKeyString.length() > HugKey.MAX_LENGTH){
            throw new KeyTooLongException("Key length must not exceed 28");
        }
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message using default seed.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     * @throws SizeTooBigException  if the message exceeding 10 Kbit.
     * @throws RangeExceededException if the seed that is used to decrypt a message
     *                                is out of inclusive range 0-36
     */
    public String decrypt(String encryptedMessage) throws SizeTooBigException, RangeExceededException{
        return decrypt(encryptedMessage, HugKey.DEFAULT_SEED);
    }



    /**
     * Decrypts an encrypted message.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     * @throws SizeTooBigException  if the message exceeding 10 Kbit.
     * @throws RangeExceededException if the seed that is used to decrypt a message
     *                                is out of inclusive range 0-36
     */
    public String decrypt (String encryptedMessage, int seed) throws RangeExceededException, SizeTooBigException{
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE ) {
            throw new RangeExceededException("Seed is out of inclusive range 0-36");
        }

        if (encryptedMessage.getBytes().length > 1250){
            //1250 bytes = 10 Kbit
            throw new SizeTooBigException("Message exceeded 10 Kbit");
        }

        String decryptedMessage = "";
        for (int i = 0; i<encryptedMessage.length(); i++) {
            char decryptedChar = (char)(encryptedMessage.charAt(i) ^
                    (( hugKeyString.charAt(i%hugKeyString.length()) ^ seed) - 'a'));
            decryptedMessage += decryptedChar;
            int a = decryptedChar;
        }
        return decryptedMessage;
    }
}
