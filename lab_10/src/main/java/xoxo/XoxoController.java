package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Fauzan A.I.
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        this.gui.setEncryptFunction(encryptMessage());
        this.gui.setDecryptFunction(decryptMessage());
    }

    /**
     * Abstract action for encrypting a message and write the message to a file,
     * also write the hug key and the seed to other file, the action will be bound
     * to GUI component.
     * @return the action
     */
    public AbstractAction encryptMessage(){
        AbstractAction encryptMessage = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption encryptor;
                XoxoMessage message;
                String seedText = gui.getSeedText();

                try {
                    encryptor = new XoxoEncryption(gui.getKeyText());
                    if (seedText.equals("DEFAULT_SEED") || seedText.equals("")){
                        message = encryptor.encrypt(gui.getMessageText());
                    } else {
                        message = encryptor.encrypt(gui.getMessageText(), Integer.parseInt(seedText));
                    }
                    writeEncryptedMessage(message.getEncryptedMessage());
                    writeHugKey(message.getHugKey());
                    writeSeed(seedText);
                    gui.appendLog("Message Encrypted");
                } catch (SizeTooBigException size) {
                    sizeTooBigExceptionCatcher(size);
                } catch (RangeExceededException range) {
                    rangeExceededExceptionCatcher(range);
                } catch (NumberFormatException number) {
                    numberFormatExceptionCatcher();
                } catch (KeyTooLongException tooLong) {
                    keyTooLongExceptionCatcher(tooLong);
                } catch (ArithmeticException arithmetic) {
                    arithmeticExceptionCatcher();
                } catch (InvalidCharacterException invalidChar) {
                    invalidCharacterExceptionCatcher(invalidChar);
                }
            }
        };
        return encryptMessage;
    }

    /**
     * Abstract action for decrypting message and write the message
     * to a file, the action will be bound to GUI component
     * @return the action
     */
    public AbstractAction decryptMessage(){
        AbstractAction decryptMessage = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoDecryption decryptor;
                String message;
                String seedText = gui.getSeedText();

                try {
                    decryptor = new XoxoDecryption(gui.getKeyText());
                    if (seedText.equals("DEFAULT_SEED") || seedText.equals("")) {
                        message = decryptor.decrypt(gui.getMessageText());
                    } else {
                        message = decryptor.decrypt(gui.getMessageText(), Integer.parseInt(seedText));
                    }
                    writeDecryptedMessage(message);
                    gui.appendLog("Message Decrypted");
                } catch (SizeTooBigException size) {
                    sizeTooBigExceptionCatcher(size);
                } catch (RangeExceededException range) {
                    rangeExceededExceptionCatcher(range);
                } catch (NumberFormatException number) {
                    numberFormatExceptionCatcher();
                } catch (KeyTooLongException tooLong) {
                    keyTooLongExceptionCatcher(tooLong);
                } catch (ArithmeticException arithmetic) {
                    arithmeticExceptionCatcher();
                }
            }
        };
        return decryptMessage;
    }

    /**
     * Method for writing encrypted message
     * @param message the message that want to be writed
     */
    private void writeEncryptedMessage(String message) {
        FileWriter fileWriter;
        try {
            String fileName = gui.getOutputNameText();
            fileName = (fileName.equals(""))?
                    "\\encrypted_message.enc":String.format("\\%s.enc", fileName);
            fileWriter = new FileWriter(System.getProperty("user.dir")
                + fileName);
            fileWriter.write(message);
            fileWriter.close();
        } catch (IOException e) {
            inputOutputExceptionCatcher();
        }
    }

    /**
     * Method for writing hug key to a file.
     * @param hugKey the hug key
     */
    private void writeHugKey(HugKey hugKey) {
        String message = hugKey.getKeyString();
        FileWriter fileWriter;
        try {
            String fileName = gui.getOutputNameText();
            fileName = (fileName.equals(""))?
                    "\\encrypted_message_hugKey.enc":String.format("\\%s_hugKey.enc", fileName);
            fileWriter = new FileWriter(System.getProperty("user.dir")
                    + fileName);
            fileWriter.write(message);
            fileWriter.close();
        } catch (IOException e) {
            inputOutputExceptionCatcher();
        }
    }

    /**
     * Method for writing seed to a file.
     * @param seed the seed
     */
    private void writeSeed(String seed) {
        FileWriter fileWriter;
        try {
            String fileName = gui.getOutputNameText();
            fileName = (fileName.equals(""))?
                    "\\encrypted_message_seed.enc":String.format("\\%s_seed.enc", fileName);
            fileWriter = new FileWriter(System.getProperty("user.dir")
                    + fileName);
            fileWriter.write(seed);
            fileWriter.close();
        } catch (IOException e) {
            inputOutputExceptionCatcher();
        }
    }

    /**
     * Method for writing decrypted message to a file.
     * @param message the message
     */
    private void writeDecryptedMessage(String message){
        FileWriter fileWriter;
        try {
            String fileName = gui.getOutputNameText();
            fileName = (fileName.equals(""))?
                    "\\decrypted_message.txt":String.format("\\%s.txt", fileName);
            fileWriter = new FileWriter(System.getProperty("user.dir")
                    + fileName);
            fileWriter.write(message);
            fileWriter.close();
        } catch (IOException e) {
            inputOutputExceptionCatcher();
        }
    }

    /**
     * Catcher for size too big exception.
     * @param size the exception
     */
    private void sizeTooBigExceptionCatcher(SizeTooBigException size){
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", size.getMessage()));
        gui.showAlertPane(size.getMessage());
    }

    /**
     * Catcher for range exceeded exception.
     * @param range the exception
     */
    private void rangeExceededExceptionCatcher(RangeExceededException range) {
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", range.getMessage()));
        gui.showAlertPane(range.getMessage());
    }

    /**
     * Catcher for key too long exception.
     * @param tooLong the exception
     */
    private void keyTooLongExceptionCatcher(KeyTooLongException tooLong) {
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", tooLong.getMessage()));
        gui.showAlertPane(tooLong.getMessage());
    }

    /**
     * Catcher for invalid character exception.
     * @param invalidChar the exception
     */
    private void invalidCharacterExceptionCatcher(InvalidCharacterException invalidChar) {
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", invalidChar.getMessage()));
        gui.showAlertPane(invalidChar.getMessage());
    }

    /**
     * Catcher for arithmetic exception.
     */
    private void arithmeticExceptionCatcher() {
        String message = "Key should not be empty!";
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", message));
        gui.showAlertPane(message);
    }

    /**
     * Catcher for number format exception.
     */
    private void numberFormatExceptionCatcher() {
        String message = "Seed should not be non-numeric!";
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", message));
        gui.showAlertPane(message);
    }

    /**
     * Catcher for IOException.
     */
    private void inputOutputExceptionCatcher() {
        String message = "Directory not found!";
        gui.appendLog(String.format("Failed to decrypt/encrypt: %s", message));
        gui.showAlertPane(message);
    }

}