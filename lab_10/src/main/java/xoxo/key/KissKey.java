package xoxo.key;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;

/**
 * The key that is required to encryt a message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Fauzan A.I.
 */
public class KissKey {

    /**
     * The Kiss Key string.
     */
    private String keyString;

    /**
     * The allowed maximum length for the Kiss Key string.
     */
    private static final int MAX_LENGTH = 28;

    /**
     * Class constructor given the string to build the Kiss Key.
     * 
     * @throws KeyTooLongException if the given key string length exceeded 28 characters.
     * @throws InvalidCharacterException if the kiss Key string  contains character
     *                                   other than A-Z, a-z, and @
     */
    public KissKey(String keyString) throws KeyTooLongException, InvalidCharacterException {
        if(keyString.length() > MAX_LENGTH){
            throw new KeyTooLongException("Key length must not exceed 28");
        }
        if(!isValidKeyString(keyString)) {
            throw new InvalidCharacterException("String key contains invalid character");
        }
        this.keyString = keyString;
    }

    /**
     * Gets a character at certain index.
     * 
     * @param i The index of a char that wants to be retrieved.
     * @return A char from the Kiss Key string at index i.
     */
    public int keyAt(int i) {
        return keyString.charAt(i % keyString.length());
    }

    /**
     * Gets the length of the Kiss Key string.
     * 
     * @return Length of the Kiss Key string.
     */
    public int length() {
        return keyString.length();
    }

    /**
     * Check whether a character is valid or not for kiss key string
     * @param character the character that want to be checked
     * @return true if valid
     */
    private static boolean isValidCharacter(char character){
        return !(character < 64 ||
                character > 122 ||
                (character > 90 &&  character < 97));
    }

    /**
     /**
     * Check whether a string is valid or not for kiss key string
     * @param keyString the string that want to be cheked
     * @return true if valid
     */
    private static boolean isValidKeyString(String keyString){
        for (int i = 0; i<keyString.length(); i++){
            if (!isValidCharacter(keyString.charAt(i))){
               return false;
            }
        }
        return true;
    }
}