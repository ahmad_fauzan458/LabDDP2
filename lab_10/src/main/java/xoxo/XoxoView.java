package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import static javax.swing.GroupLayout.Alignment.LEADING;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Fauzan A.I.
 */
public class XoxoView extends JFrame{

    /**
     * Label next to the place of key field
     */
    private JLabel keyLabel;

    /**
     * Label next to the place of seed field
     */
    private JLabel seedLabel;

    /**
     * Label next to the place of message field
     */
    private JLabel messageLabel;

    /**
     * Label next to the output name field
     */
    private JLabel outputNameLabel;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the
     * name of the output.
     */
    private JTextField outputNameField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logArea;

    /**
     * Scroll pane for log area.
     */
    private JScrollPane scrollPane;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     * The GUI using groupLayout with some component
     */
    private void initGui() {
        //Initiate all label
        keyLabel = new JLabel("Key: " );
        seedLabel = new JLabel("Seed: ");
        messageLabel = new JLabel("Message: ");
        outputNameLabel = new JLabel("Output Name: ");

        //Initiate all field
        keyField = new JTextField(20);
        seedField = new JTextField(20);
        messageField = new JTextField(20);
        outputNameField = new JTextField(20);

        //Initiate all button
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");

        //Initiate log area
        logArea = new JTextArea(15, 20);
        logArea.setEditable(false);
        logArea.setLineWrap(true);
        logArea.setWrapStyleWord(true);

        //Initiate scroll pane
        scrollPane = new JScrollPane(logArea);
        scrollPane.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setWheelScrollingEnabled(true);
        scrollPane.setPreferredSize(new Dimension(200, 200));

        //Initiate group layout
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        //Set the group layout's horizontal group
        layout.setHorizontalGroup(layout.createParallelGroup(LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(LEADING)
                                .addComponent(keyLabel)
                                .addComponent(seedLabel)
                                .addComponent(messageLabel)
                                .addComponent(outputNameLabel))
                        .addGroup(layout.createParallelGroup(LEADING)
                                .addComponent(keyField)
                                .addComponent(seedField)
                                .addComponent(messageField)
                                .addComponent(outputNameField))
                        .addGroup(layout.createParallelGroup(LEADING)
                                .addComponent(encryptButton)
                                .addComponent(decryptButton)))
                .addComponent(scrollPane)
        );

        //Set the group layout's vertical group
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(LEADING)
                                        .addComponent(keyLabel)
                                        .addComponent(keyField))
                                .addGroup(layout.createParallelGroup(LEADING)
                                        .addComponent(seedLabel)
                                        .addComponent(seedField))
                                .addGroup(layout.createParallelGroup(LEADING)
                                        .addComponent(messageLabel)
                                        .addComponent(messageField))
                                .addGroup(layout.createParallelGroup(LEADING)
                                        .addComponent(outputNameLabel)
                                        .addComponent(outputNameField)))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(encryptButton)
                                .addComponent(decryptButton)))
                .addComponent(scrollPane)
        );

        this.setTitle("Xoxo Crypto");
        this.pack();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Gets the output name text from the output name field.
     * @return The output name
     */
    public String getOutputNameText() {
        return outputNameField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logArea.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * Show an alert pane with desired mesage.
     * It will be used when an error occured.
     * @param message the error message.
     */
    public void showAlertPane(String message){
        JOptionPane.showMessageDialog(this, message);
    }
}