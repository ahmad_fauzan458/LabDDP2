package xoxo.exceptions;

/**
 * An exception that is thrown if the seed that
 * is used to decrypt a message is out of inclusive
 * range 0-36
 *
 * @author Ahmad Fauzan A.I.
 */
public class RangeExceededException extends IllegalArgumentException{

    public RangeExceededException(String message){
        super(message);
    }

}