package xoxo.exceptions;

/**
 * An exception that is thrown if the message
 * exceeding 10 Kbit.
 *
 * @author Ahmad Fauzan A.I.
 */
public class SizeTooBigException extends IllegalArgumentException{

    public SizeTooBigException (String message){
        super(message);
    }
    
}