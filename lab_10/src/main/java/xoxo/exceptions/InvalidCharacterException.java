package xoxo.exceptions;

/**
 * An exception that is thrown if the Kiss Key that
 * is used to encrypt a message contains character
 * other than A-Z, a-z, and @
 *
 * @author Ahmad Fauzan A.I.
 */
public class InvalidCharacterException extends IllegalArgumentException{

    private final static String validCharacter =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@";

    public static String getValidCharacter() {
        return validCharacter;
    }

    public InvalidCharacterException(String message){
        super(message);
    }

}