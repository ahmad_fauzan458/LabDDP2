package character;

public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, "Magician", hp);
        this.weakness = 2;
    }

    /**
     * Method untuk membakar player lain.
     * @param enemy lawan yang ingin dibakar.
     */
    public void burn(Player enemy) {
        int damage = 10 * enemy.weakness;
        enemy.setHp(enemy.hp - damage);
        enemy.setAlive();
        if (!enemy.alive) {
            enemy.burnt = true;
        }
    }

}
