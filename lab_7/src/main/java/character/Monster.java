package character;

public class Monster extends Player {
    private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int hp) {
        super(name, "Monster", 2 * hp);
    }

    public Monster(String name, int hp, String roar) {
        this(name, hp);
        this.roar = roar;
    }

    /**
     * Method untuk membuat Monster melakukan roar.
     * @return bunyi roar monster.
     */
    public String roar() {
        return this.roar;
    }
}