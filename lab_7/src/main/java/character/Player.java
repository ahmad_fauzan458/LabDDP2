package character;

public class Player {
    protected String name;
    protected int hp;
    protected String type;
    protected String diet = "";
    protected boolean burnt = false;
    protected boolean alive = true;
    protected int weakness = 1;

    /**
     * Player constructor.
     * Player yang dinisiasi langsung di cek sudah meninggal atau belum.
     * @param name nama dari object Player yang ingin dinisiasi.
     * @param type tipe dari object Player yang ingin dinisiasi.
     * @param hp hp dari object Player yang ingin dinisiasi.
     */
    public Player(String name, String type, int hp) {
        this.name = name;
        this.hp = hp;
        this.type = type;
        this.setAlive();
    }

    /**
     * Method untuk mengembaikan nama Player.
     * @return nama Player.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Fungsi untuk mengembalikan status dari Player.
     * Meliputi type, nama, hp, status hidup, diet.
     * @return status dari player.
     */
    public String getStatus() {
        String lifeStatus = this.alive ? "Masih hidup" :
                "Sudah meninggal dunia dengan damai";
        String playerDiet;
        if (this.getDiet().equals("Belum memakan siapa siapa")) {
            playerDiet = this.getDiet();
        } else {
            playerDiet = String.format("Memakan %s", this.getDiet());
        }
        return String.format("%s %s\nHP: %d\n%s\n%s", this.type,
                this.name, this.hp, lifeStatus, playerDiet);
    }

    /**
     * Method untuk mengembalikan informasi diet Player.
     * Jika belum ada, kembalikan informasi belum memakan siapa siapa.
     * @return informasi diet Player.
     */
    public String getDiet() {
        String info;
        if (this.diet == "") {
            info = "Belum memakan siapa siapa";
        } else {
            info = this.diet;
        }
        return info;
    }

    /**
     * Method untuk mengembalikan hp Player.
     * @return hp Player.
     */
    public int getHp() {
        return this.hp;
    }

    /**
     * Method untuk mengembalikan tipe player.
     * @return tipe player.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Method untuk mengembalikan boolean kehidupan player.
     * @return boolean status hidup player.
     */
    public boolean isAlive() {
        return this.alive;
    }

    /**
     * Method untuk menambahkan Player lain yang sudah dimakan
     * ke dalam diet Player.
     * @param enemy target makan.
     */
    public void setDiet(Player enemy) {
        if (!this.diet.equals("")) {
            this.diet = ", " + this.diet;
        }
        this.diet = String.format("%s %s", enemy.type, enemy.name) + this.diet;
    }

    /**
     * Method untuk mengatur status hidup Player.
     * Player tidak hidup jika hp <= 0;
     */
    public void setAlive() {
        if (this.hp <= 0) {
            this.alive = false;
        }
    }

    /**
     * Method untuk mengatur hp Player.
     * Jika hp <= 0, maka hp diatur menjadi 0.
     */
    public void setHp(int hp) {
        this.hp = hp <= 0 ? 0 : hp;
    }

    /**
     * Method untuk melakukan serangan ke Player lain.
     * Damage bergantung pada weakness enemy.
     * Memanggil method untuk mengatur status hidup lawan.
     * @param enemy lawan yang ingin diserang.
     */
    public void attack(Player enemy) {
        int damage = 10 * enemy.weakness;
        enemy.setHp(enemy.hp - damage);
        this.setAlive();
    }

    /**
     * Method untuk memakan Player lain.
     * Memanggil method untuk mengatur diet Player.
     * @param enemy target yang ingin dimakan.
     */
    public void eat(Player enemy) {
        this.hp += 15;
        this.setDiet(enemy);
    }

    /**
     * Method untuk mengecek apakah Player lain bisa
     * dimakan atau tidak.
     * @param player Player yang ingin dimakan.
     * @return bisa dimakan atau tidak.
     */
    public boolean canEat(Player player) {
        return !player.alive;
    }
}

