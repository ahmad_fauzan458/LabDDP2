package character;

public class Human extends Player {
    public Human(String name, int hp) {
        super(name, "Human", hp);
    }

    public Human(String name, String type, int hp) {
        super(name, type, hp);
    }

    /**
     * Method untuk mengecek Player lain bisa dimakan
     * atau tidak.
     * @param player Player yang ingin dimakan.
     * @return bisa dimakan atau tidak.
     */
    public boolean canEat(Player player) {
        return player.type.equals("Monster") && player.burnt;
    }

    /**
     * Method untuk memakan Player lain.
     * @param enemy target yang ingin dimakan.
     */
    public void eat(Player enemy) {
        if (canEat(enemy)) {
            super.eat(enemy);
        }
    }
}