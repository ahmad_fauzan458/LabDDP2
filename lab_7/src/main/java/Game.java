import character.Human;
import character.Magician;
import character.Monster;
import character.Player;

import java.util.ArrayList;

public class Game {
    ArrayList<Player> players = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter.
     *
     * @param name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game.
     *
     * @param chara nama karakter yang ingin ditambahkan
     * @param tipe tipe dari karakter yang ingin ditambahkan
     *             terdiri dari monster, magician dan human
     * @param hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter
     *             contoh "Sudah ada karakter bernama chara"
     *             atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        String info;
        if (find(chara) == null) {
            switch (tipe) {
                case "Monster":
                    players.add(new Monster(chara, hp));
                    break;
                case "Magician":
                    players.add(new Magician(chara, hp));
                    break;
                default: // case "Human"
                    players.add(new Human(chara, hp));
            }
            info = String.format("%s ditambah ke game", chara);
        } else {
            info = String.format("Sudah ada karakter bernama %s", chara);
        }
        return info;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar,
     * roar hanya bisa dilakukan oleh monster.
     *
     * @param chara nama karakter yang ingin ditambahkan
     * @param tipe tipe dari karakter yang ingin ditambahkan
     *             terdiri dari monster, magician dan human
     * @param hp hp dari karakter yang ingin ditambahkan
     * @param roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter
     *             contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        String info;
        if (!tipe.equals("Monster")) {
            info = "Tipe tidak sesuai";
        } else if (find(chara) == null) {
            players.add(new Monster(chara, hp, roar));
            info = String.format("%s ditambah ke game", chara);
        } else {
            info = String.format("Sudah ada karakter bernama %s", chara);
        }
        return info;
    }

    /**
     * fungsi untuk menghapus character dari game.
     *
     * @param chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player player = find(chara);
        String info;
        if (player != null) {
            players.remove(player);
            info = String.format("%s dihapus dari game", chara);
        } else {
            info = String.format("Tidak ada %s", chara);
        }
        return info;
    }


    /**
     * fungsi untuk menampilkan status character dari game.
     *
     * @param chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        String info;
        Player player = find(chara);
        if (player == null) {
            info = String.format("Tidak ada %s", chara);
        } else {
            info = player.getStatus();
        }
        return info;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game.
     *
     * @return String result nama dari semua character,
     *             format sesuai dengan deskripsi soal atau contoh output.
     */
    public String status() {
        String info = "";
        for (Player player : players) {
            info += player.getStatus() + "\n";
        }
        return info;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara.
     *
     * @param chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        String info;
        Player player = find(chara);
        if (player == null) {
            info = String.format("Tidak ada %s", chara);
        } else {
            info = player.getDiet();
        }
        return info;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game.
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String info = "Termakan : ";
        String awal = "Termakan : ";
        for (Player player : players) {
            if (player.getDiet().equals("Belum memakan siapa siapa")) {
                continue;
            }
            if (!info.equals(awal)) {
                info += ", ";
            }
            info += player.getDiet();
        }
        return info;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName.
     *
     * @param meName nama dari character yang sedang dimainkan.
     * @param enemyName nama dari character yang akan di serang.
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal.
     */
    public String attack(String meName, String enemyName) {
        Player player1 = find(meName);
        Player player2 = find(enemyName);
        String info;
        if (player1 == null || player2 == null) {
            info = String.format("Tidak ada %s atau %s", meName, enemyName);
        } else if (!player1.isAlive()) {
            info = String.format("%s tidak bisa menyerang %s", meName, enemyName);
        } else {
            player1.attack(player2);
            info = String.format("Nyawa %s %d", enemyName, player2.getHp());
        }
        return info;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName.
     * Method ini hanya boleh dilakukan oleh magician.
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName n
     *
     *
     *
     *                  ama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player player1 = find(meName);
        Player player2 = find(enemyName);
        Magician magician;
        String info;
        if (player1 == null || player2 == null) {
            info = String.format("Tidak ada %s atau %s", meName, enemyName);
        } else if (!player1.isAlive() || !player1.getType().equals("Magician")) {
            info = String.format("%s tidak bisa membakar %s", meName, enemyName);
        } else {
            magician = (Magician) (player1);
            magician.burn(player2);
            info = String.format("Nyawa %s %d", enemyName, player2.getHp());
            if (!player2.isAlive()) {
                info += " \n dan matang";
            }
        }
        return info;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName.
     * enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal.
     * @param meName nama dari character yang sedang dimainkan
     * @param enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player player1 = find(meName);
        Player player2 = find(enemyName);
        Human human;
        String info;
        boolean canEat;
        if (player1 == null || player2 == null) {
            info = String.format("Tidak ada %s atau %s", meName, enemyName);
        } else if (!player1.isAlive()) {
            info = String.format("%s tidak bisa memakan %s", meName, enemyName);
        } else {
            if (player1.getType().equals("Human")
                    || player1.getType().equals("Magician")) {
                human = (Human) (player1);
                canEat = human.canEat(player2);
            } else {
                canEat = player1.canEat(player2);
            }

            if (canEat) {
                player1.eat(player2);
                info = String.format("%s memakan %s\nNyawa %s kini %d",
                        meName, enemyName, meName, player1.getHp());
                players.remove(player2);
            } else {
                info = String.format("%s tidak bisa memakan %s",
                        meName, enemyName);
            }
        }
        return info;
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player player = find((meName));
        String info;
        if (player == null) {
            info = String.format("Tidak ada %s", meName);
        } else if (!player.getType().equals("Monster")) {
            info = String.format("%s tidak bisa berteriak", meName);
        } else {
            Monster monster = (Monster) (player);
            info = monster.roar();
        }
        return info;
    }
}