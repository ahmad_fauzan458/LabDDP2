import java.util.Scanner;
public class MultiPlayer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] playerRegistration = scanner.nextLine().split(" ");
        int amountOfPlayer = Integer.parseInt(playerRegistration[0]);
        Player[] playersArray = new Player[amountOfPlayer];

        /*
        Menginstansiasi objek Number untuk setiap angka dari input
        Kemudian memasukkannya ke dua dimensi array (numbers) berurutan sesuai index papan bingo
        Dan memasukkan ke satu dimensi array (numberStates) dengan nilainya menjadi index
        Kemudian melakukan instansiasi objek BingoCard dengan numbers dan numberStates
        Kemudian melakukan instansiasi objek Player dengan nama pemain dan objek BingoCard, sebanyak jumlah pemain
         */

        for (int playerIndex=0; playerIndex<amountOfPlayer; playerIndex++) {
            Number[][] numbers = new Number[5][5];
            Number[] numberStates = new Number[100];
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    int value = scanner.nextInt();
                    Number card = new Number(value, i, j);
                    numbers[i][j] = card;
                    numberStates[value] = card;
                }
            }
            playersArray[playerIndex] = new Player(playerRegistration[playerIndex+1], new BingoCard(numbers, numberStates));
        }

        //Mengeksekusi program sesuai perintah
        boolean flag = true;
        String[] command;
        String perintahLanjutan;

        boolean bingo = false;
        while (!bingo) {
            command = scanner.nextLine().split(" ");

            switch(command[0].toUpperCase()){
                case "MARK":
                    BingoCard.markNum(playersArray, Integer.parseInt(command[1]));
                    bingo = BingoCard.Bingo(playersArray);
                    break;

                case "INFO":
                    BingoCard.info(playersArray, command[1]);
                    break;

                case "RESTART":
                    BingoCard.restart(playersArray, command[1]);
                    break;
            }
        }
    }
}
