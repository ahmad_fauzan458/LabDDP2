public class Player {

    private String name;
    private BingoCard bingoCard;
    private boolean restartAvailibility = true;

    public Player(String name, BingoCard bingoCard){
        this.name = name;
        this.bingoCard = bingoCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BingoCard getBingoCard() {
        return bingoCard;
    }

    public void setBingoCard(BingoCard bingoCard) {
        this.bingoCard = bingoCard;
    }

    public boolean isRestartAvailibility() {
        return restartAvailibility;
    }

    public void setRestartAvailibility(boolean restartAvailibility) {
        this.restartAvailibility = restartAvailibility;
    }

}
