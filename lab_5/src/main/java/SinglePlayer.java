import java.util.Scanner;
public class SinglePlayer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Number[][] numbers = new Number[5][5];
        Number[] numberStates = new Number[100];

        /*
        Menginstansiasi objek Number untuk setiap angka dari input
        Kemudian memasukkannya ke dua dimensi array (numbers) berurutan sesuai index papan bingo
        Dan memasukkan ke satu dimensi array (numberStates) dengan nilainya menjadi index
         */
        for (int i = 0; i<5; i++)
            for (int j = 0; j<5; j++) {
                int value = scanner.nextInt();
                Number card = new Number(value, i, j);
                numbers[i][j] = card;
                numberStates[value] = card;
            }

        // Instansiasi kelas BingoCard
        BingoCard bingoCard = new BingoCard(numbers, numberStates);

        //Mengeksekusi program sesuai perintah
        boolean bingo = false;
        while (!bingo) {
            String[] command = scanner.nextLine().split(" ");
            switch(command[0].toUpperCase()){
                case "MARK":
                    System.out.println(bingoCard.markNum(Integer.parseInt(command[1])));
                    bingo = bingoCard.Bingo();
                    break;
                case "INFO":
                    System.out.println(bingoCard.info());
                    break;
                case "RESTART":
                    bingoCard.restart();
                    break;
            }
        }
    }
}
