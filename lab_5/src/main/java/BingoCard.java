/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Anda diwajibkan untuk mengimplementasikan method yang masih kosong
 * Anda diperbolehkan untuk menambahkan method jika dibutuhkan
 * HINT : Bagaimana caranya cek apakah sudah menang atau tidak? Mungkin dibutuhkan method yang bisa membantu? Hmmmm.
 * Semangat ya :]
 *
 * Code Author (Mahasiswa):
 * @author Ahmad Fauzan Amirul Isnain, NPM 1706979152, Kelas DDP-2 D, GitLab Account: ahmad_fauzan458
 */

public class BingoCard {

    private Number[][] numbers;
    private Number[] numberStates;
    private boolean isBingo;

    /**
     * BingoCard constructor
     * @param numbers array dua dimensi untuk papan bingo
     * @param numberStates array satu dimensi untuk mempermudah akses Number menggunakan valuenya
     */
    public BingoCard(Number[][] numbers, Number[] numberStates) {
        this.numbers = numbers;
        this.numberStates = numberStates;
        this.isBingo = false;
    }

    public Number[][] getNumbers() {
        return numbers;
    }

    public void setNumbers(Number[][] numbers) {
        this.numbers = numbers;
    }

    public Number[] getNumberStates() {
        return numberStates;
    }

    public void setNumberStates(Number[] numberStates) {
        this.numberStates = numberStates;
    }

    public boolean isBingo() {
        return isBingo;
    }

    public void setBingo(boolean isBingo) {
        this.isBingo = isBingo;
    }

    /**
     * Method dengan parameter sebuah angka int, untuk menandai kartu yang memiliki angka tersebut
     * @param num angka yang ingin ditandai
     * @return informasi tentang kondisi kartu dengan angka yang ingin ditandai
     * (tidak ada/sudah tersilang/berhasil tersilang)
     */
    public String markNum(int num){
        String markNumInfo = "";
        if(this.numberStates[num] == null) {
            markNumInfo += String.format("Kartu tidak memiliki angka %d", num);
        }
        else if(this.numberStates[num].isChecked()) {
            markNumInfo += String.format("%d sebelumnya sudah tersilang", num);
        }
        else {
            this.numberStates[num].setChecked(true);
            this.checkBingo(this.numberStates[num]);
            markNumInfo += String.format("%d tersilang", num);
        }
        return markNumInfo;

    }

    /**
     * Method dengan parameter sebuah angka int, untuk menandai kartu yang memiliki angka tersebut
     * pada setiap pemain di multiplayer mode
     * @param playersArray Array berisi pemain
     * @param num angka yang ingin ditandai
     */
    public static void markNum(Player[] playersArray, int num){
        for (Player player : playersArray){
            System.out.printf("%s : ", player.getName());
            System.out.println(player.getBingoCard().markNum(num));
        }
    }

    /**
     * Method untuk mencetak kondisi papan bingo, kartu yang sudah ditandai akan menjadi "X"
     * @return kondisi papan bingo saat ini
     */
    public String info(){
        String info = "";
        for (int i=0; i<5; i++){
            for (int j=0; j<5; j++) {
                if (this.numbers[i][j].isChecked()) info += "| X  ";
                else info += String.format("| %d ", this.numbers[i][j].getValue());
            }
            info += "|";
            if (i != 4) info += "\n";
        }
        return info;
    }

    /**
     * Method untuk mencetak kondisi papan bingo, kartu yang sudah ditandai akan menjadi "X"
     * Digunakan pada multiplayer mode
     * @param playersArray Array berisi pemain
     * @param name Nama pemain yang dicek papan bingonya
     */
    public static void info(Player[] playersArray, String name){
        for (Player player:playersArray){
            if (player.getName().equals(name)){
                System.out.printf("%s\n", player.getName());
                System.out.println(player.getBingoCard().info());
            }
        }
    }

    /**
     * Method untuk merestart papan bingo
     * Semua kondisi kartu kembali menjadi seperti semula (menjadi tidak ada yang ditandai)
     */
    public void restart(){
        for (int i=0; i<5; i++)
            for (int j = 0; j<5; j++) {
                this.numbers[i][j].setChecked(false);
            }
    }

    /**
     * Method untuk merestart papan bingo pada semua pemain di mode multiplayer
     * @param playersArray Array berisi pemain
     * @param name nama pemain yang melakukan restart
     */
    public static void restart(Player[] playersArray, String name){
        boolean restart = false;

        for (Player player:playersArray) {
            if (player.getName().equals(name)) {
                if (player.isRestartAvailibility()) {
                    player.setRestartAvailibility(false);
                    restart = true;
                    break;
                }
                else {
                    System.out.printf("%s sudah pernah mengajukan RESTART\n", player.getName());
                    return;
                }
            }
        }

        if(restart) {
            for (Player player : playersArray) {
                player.getBingoCard().restart();
            }
            System.out.println("Mulligan!");
        }
    }


    /**
     * Mengecek kondisi kemenangan untuk horizontal, vertical, dan diagonal
     * Apabila kondisi kemenangan terpenuhi akan memanggil method Bingo()
     */
    public void checkBingo(Number number){
        boolean row = true;
        boolean column = true;
        boolean diagonal1 = true;
        boolean diagonal2 = true;

        for (int i=0, j=4; i<5; i++, j--){
            row &= this.numbers[number.getX()][i].isChecked();
            column &= this.numbers[i][number.getY()].isChecked();
            diagonal1 &= this.numbers[i][i].isChecked();
            diagonal2 &= this.numbers[i][j].isChecked();
        }

        if (diagonal1||diagonal2||row||column) {
            this.isBingo = true;
        }

    }

    /**
     * Method untuk menghentikan permainan apabila sudah menang
     * Mencetak "BINGO!" dan kondisi terakhir papan bingo
     * @return boolean true jika Bingo, false jika tidak
     */
    public boolean Bingo(){
        if(this.isBingo) {
            System.out.println("BINGO!");
            System.out.println(this.info());
            return true;
        }
        return false;
    }

    /**
     * Method untuk menghentikan permainan apabila terdapat pemain menang
     * Mencetak "BINGO!" dan kondisi terakhir papan bingo untuk setiap pemain yang menang
     * @return boolean true jika terdapat pemain yang Bingo, false jika tidak
     */
    public static boolean Bingo(Player[] playersArray){
        boolean bingo = false;
        for (Player player:playersArray){
            if (player.getBingoCard().isBingo){
                System.out.printf("%s : ", player.getName());
                System.out.println("BINGO!");
                System.out.println(player.getBingoCard().info());
                bingo = true;
            }
        }
        return bingo;
    }
}
