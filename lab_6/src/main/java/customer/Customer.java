package customer;

import java.util.ArrayList;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;


public class Customer {
    private String name;
    private String gender;
    private int age;
    private ArrayList<Ticket> tickets = new ArrayList<>();

    public Customer(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Method untuk memesan tiket sesuai keinginan customer.
     * @param theater theater yang ingin dipesan tiket filmnya
     * @param title judul dari film yang ingin dipesan tiketnya
     * @param showtime jadwal tayang dari film yang ingin dipesan tiketnya
     * @param typeOfTicket jenis film yang ingin ditonton (Biasa/3 Dimensi)
     * @return jika berhasil memesan, mengembalikan object Ticket sesuai pesanan
     *         jika tidak, mengembalikan null
     */
    public Ticket orderTicket(Theater theater, String title, String showtime, String typeOfTicket) {
        boolean threeDimensionTicket = typeOfTicket.equals("3 Dimensi");
        Ticket orderedTicket = null;

        for (Ticket ticket:theater.getTickets()) {
            if (ticket.getMovie().getTitle().equals(title)
                    && ticket.getShowtime().equals(showtime)
                    && ticket.isThreeDimensionFilm() == threeDimensionTicket) {
                orderedTicket = ticket;
                break;
            }
        }

        if (orderedTicket == null) {
            System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia "
                            + "di %s\n", title, typeOfTicket, showtime, theater.getName());
        } else if (this.age < orderedTicket.getMovie().getAgeRating()) {
            System.out.printf("%s masih belum cukup umur untuk menonton %s dengan "
                    + "rating %s\n", this.name, title, orderedTicket.getMovie().getAgeCategory());
            orderedTicket = null;
        } else {
            System.out.printf("%s telah membeli tiket %s jenis %s di %s "
                            + "pada hari %s seharga Rp. %d\n", this.name, title, typeOfTicket,
                    theater.getName(), showtime, orderedTicket.getHarga());
            theater.setBalance(theater.getBalance() + orderedTicket.getHarga());
            theater.getTickets().remove(orderedTicket);
            this.tickets.add(orderedTicket);
        }

        return orderedTicket;
    }

    /**
     * Method untuk mencari film sesuai keinginan customer.
     * @param theater theater yang ingin dilihat ketersediaan filmnya
     * @param title judul dari film yang dicari
     */
    public void findMovie(Theater theater, String title) {
        for (Ticket ticket:theater.getTickets()) {
            if (ticket.getMovie().getTitle().equals(title)) {
                ticket.getMovie().printInfo();
                return;
            }
        }
        System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n",
                title, this.name, theater.getName());
    }

    /**
     * Method untuk membatalkan tiket dan mengembalikannya ke bioskop.
     * Tiket bisa dikembalikan jika film terdaftar di bioskop tempat pengembalian,
     * tiket belum digunakan, dan saldo kas bioskop mencukupi.
     * @param theater bioskop tempat membatalkan tiket
     */
    public void cancelTicket(Theater theater) {
        Ticket mostRecentTicket = this.tickets.get(tickets.size() - 1);
        boolean movieAvailability = false;

        for (Movie movie:theater.getMovies()) {
            if (mostRecentTicket.getMovie().equals(movie)) {
                movieAvailability = true;
                break;
            }
        }

        if (!movieAvailability) {
            System.out.printf("Maaf tiket tidak bisa dikembalikan, %s tidak tersedia dalam %s\n",
                    mostRecentTicket.getMovie().getTitle(), theater.getName());
        } else if (!mostRecentTicket.isTicketCondition()) {
            this.tickets.remove(mostRecentTicket);

            System.out.printf("Tiket tidak bisa dikembalikan karena film %s sudah ditonton"
                    + " oleh %s\n", mostRecentTicket.getMovie().getTitle(), this.name);
        } else if (theater.getBalance() < mostRecentTicket.getHarga()) {
            System.out.printf("Maaf ya tiket tidak bisa dibatalkan, "
                            + "uang kas di bioskop %s lagi tekor...\n", theater.getName());
        } else {
            this.tickets.remove(mostRecentTicket);
            theater.getTickets().add(mostRecentTicket);
            theater.setBalance(theater.getBalance() - mostRecentTicket.getHarga());

            String typeOfTicket = mostRecentTicket.isThreeDimensionFilm() ? "3 Dimensi" : "Biasa";
            System.out.printf("Tiket film %s dengan waktu tayang %s jenis %s dikembalikan"
                    + " ke bioskop %s\n", mostRecentTicket.getMovie().getTitle(),
                    mostRecentTicket.getShowtime(), typeOfTicket, theater.getName());
        }
    }

    /**
     * Method untuk menonton film dengan menggunakan ticket.
     * @param ticket ticket yang ingin digunakan untuk menonton film
     */
    public void watchMovie(Ticket ticket) {
        if (ticket.isTicketCondition()) {
            System.out.printf("%s telah menonton film %s\n",
                    this.name, ticket.getMovie().getTitle());
            ticket.setTicketCondition(false);
        } else {
            System.out.println("Tiket sudah pernah digunakan");
        }
    }

}
