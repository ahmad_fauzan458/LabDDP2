package theater;

import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;

public class Theater {
    private String name;
    private int balance;
    private ArrayList<Ticket> tickets;
    private Movie[] movies;

    public Theater(String name, int balance, ArrayList<Ticket> tickets, Movie[] movies) {
        this.name = name;
        this.balance = balance;
        this.tickets = tickets;
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Movie[] getMovies() {
        return movies;
    }

    public void setMovies(Movie[] movies) {
        this.movies = movies;
    }

    public int numberOfTickets() {
        return tickets.size();
    }

    /**
     * Method untuk mencetak total pendapatan yang didapatkan dari beberapa theater.
     * @param theaters daftar theater yang ingin di cetak pendapatannya
     */
    public static void printTotalRevenueEarned(Theater[] theaters) {
        System.out.printf("Total uang yang dimiliki Koh Mas : Rp. %s\n", totalRevenue(theaters));
        System.out.println("------------------------------------------------------------------");
        for (Theater theater : theaters) {
            theater.printRevenueEarned();
        }
        System.out.println("------------------------------------------------------------------");
    }

    /**
     * Method untuk menjumlahkan pendapatan dari beberapa theater.
     * @param theaters daftar theater yang ingin dijumlahkan pendapatannya
     * @return total pendapatan dari beberapa theater
     */
    private static int totalRevenue(Theater[] theaters) {
        int totalRevenue = 0;
        for (Theater theater:theaters) {
            totalRevenue += theater.balance;
        }
        return totalRevenue;
    }

    /**
     * Method untuk mencetak saldo dari sebuah theater.
     */
    private void printRevenueEarned() {
        System.out.printf("Bioskop     : %s\n", this.name);
        System.out.printf("Saldo Kas   : Rp. %s\n\n", this.balance);
    }

    /**
     *  Mencetak info dari sebuah theater meliputi nama, saldo, jumlah tiket tersedia,
     *  dan daftar film tersedia.
     */
    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.printf("Bioskop                 : %s\n", this.name);
        System.out.printf("Saldo Kas               : %s\n", this.balance);
        System.out.printf("Jumlah tiket tersedia   : %s\n", this.numberOfTickets());
        System.out.printf("Daftar Film tersedia    : %s\n", this.movieList());
        System.out.println("------------------------------------------------------------------");
    }

    /**
     * Method untuk mendata film dari sebuah theater.
     * @return daftar film dari suatu theater
     */
    private String movieList() {
        String movieList = "";
        for (int i = 0; i < this.movies.length; i++) {
            movieList += this.movies[i].getTitle();
            if (i != this.movies.length - 1) {
                movieList += ", ";
            }
        }
        return movieList;
    }
}
