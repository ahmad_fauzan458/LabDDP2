package movie;

public class Movie {
    private String title;
    private String genre;
    private int duration; //dalam menit
    private String ageCategory;
    private String originType;

    public Movie(String title, String ageCategory, int duration, String genre, String originType) {
        this.title = title;
        this.genre = genre;
        this.duration = duration;
        this.ageCategory = ageCategory;
        this.originType = originType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(String ageCategory) {
        this.ageCategory = ageCategory;
    }

    public String getOriginType() {
        return originType;
    }

    public void setOriginType(String originType) {
        this.originType = originType;
    }

    /**
     * Method untuk mendapatkan umur minimal menonton film sesuai dengan kategori film.
     * @return umur minimal menonton film
     */
    public int getAgeRating() {
        int ageRating;
        switch (this.ageCategory) {
            case "Dewasa":
                ageRating = 17;
                break;
            case "Remaja":
                ageRating = 13;
                break;
            default: //case "Umum"
                ageRating = 0;
        }
        return ageRating;
    }

    /**
     * Method untuk mengecek kesamaan film.
     * @param otherMovie film lain yang ingin dibandingkan kesamaannya
     * @return true jika judul dari kedua film sama, false jika tidak
     */
    public boolean equals(Movie otherMovie) {
        if (this.title.equals(otherMovie.title)) {
            return true;
        }
        return false;
    }

    /**
     * Method untuk mencetak info dari film meliputi judul, genre, durasi, rating, dan jenis.
     */
    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.printf("Judul   : %s\n", this.title);
        System.out.printf("Genre   : %s\n", this.genre);
        System.out.printf("Durasi  : %d menit\n", this.duration);
        System.out.printf("Rating  : %s\n", this.ageCategory);
        System.out.printf("Jenis   : Film %s\n", this.originType);
        System.out.println("------------------------------------------------------------------");

    }



}
