package ticket;

import movie.Movie;

public class Ticket {
    private Movie movie;
    private String showtime;
    private boolean threeDimensionFilm;
    private boolean ticketCondition = true;

    public Ticket(Movie movie, String showtime, boolean threeDimensionFilm) {
        this.movie = movie;
        this.showtime = showtime;
        this.threeDimensionFilm = threeDimensionFilm;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getShowtime() {
        return showtime;
    }

    public void setShowtime(String showtime) {
        this.showtime = showtime;
    }

    public boolean isThreeDimensionFilm() {
        return threeDimensionFilm;
    }

    public void setThreeDimensionFilm(boolean threeDimensionFilm) {
        this.threeDimensionFilm = threeDimensionFilm;
    }

    public boolean isTicketCondition() {
        return ticketCondition;
    }

    public void setTicketCondition(boolean ticketCondition) {
        this.ticketCondition = ticketCondition;
    }

    /**
     * Method untuk menghitung harga tiket sesuai ketentuan.
     * @return harga tiket
     */
    public int getHarga() {
        int harga = 60000;
        if (this.showtime.equals("Sabtu") || this.showtime.equals("Minggu")) {
            harga += 40000;
        }
        if (this.threeDimensionFilm) {
            harga += 0.2 * harga;
        }
        return harga;
    }

    /**
     * Mencetak informasi dari sebuah tiket, meliputi judul film, jadwal tayang,
     * dan jenis tiket(Biasa/3 Dimensi).
     */
    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.printf("Film            : %s\n",this.movie.getTitle());
        System.out.printf("Jadwal Tayang   : %s\n",this.showtime);
        System.out.printf("Jenis           : %s\n",this.threeDimensionFilm ? "3 Dimensi" : "Biasa");
        System.out.println("------------------------------------------------------------------");
    }

}
