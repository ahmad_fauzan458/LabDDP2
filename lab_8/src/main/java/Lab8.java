import java.util.Scanner;

import karyawan.Intern;
import karyawan.Karyawan;
import karyawan.Manager;
import karyawan.Staff;
import karyawan.Superior;

/**
 * Main class for execute Lab8 Program.
 */
class Lab8 {

    public static void main(String[] args) {
        Korporasi korporasi = new Korporasi("PT. TAMPAN");
        Scanner scanner = new Scanner(System.in);
        boolean hasStaffSalaryLimit = false;
        while (!hasStaffSalaryLimit) {
            try {
                int salaryLimit = Integer.parseInt(scanner.nextLine());
                Staff.setSalaryLimit(salaryLimit);
                hasStaffSalaryLimit = true;
                System.out.printf("Batas gaji staff diatur menjadi %s\n", salaryLimit);
            } catch (Exception e) {
                System.out.println("Masukkan batas untuk gaji staff");
            }
        }

        while (true) {
            String[] command = scanner.nextLine().split(" ");
            try {
                switch (command[0]) {
                    case "TAMBAH_KARYAWAN": {
                        String type = command[1];
                        String name = command[2];
                        double salary = Double.parseDouble(command[3]);
                        System.out.println(addEmployee(type, name, salary, korporasi));
                        break;
                    }
                    case "STATUS": {
                        String name = command[1];
                        System.out.println(employeeStatus(name, korporasi));
                        break;
                    }

                    case "TAMBAH_BAWAHAN": {
                        String superiorName = command[1];
                        String subordinateName = command[2];
                        System.out.println(addSubordinate(superiorName, subordinateName, korporasi));
                        break;
                    }

                    case "GAJIAN": {
                        System.out.println(payday(korporasi));
                        break;
                    }

                    default: {
                        System.out.println("Input tidak valid");
                    }
                }
            } catch (Exception e) {
                System.out.println("Input tidak valid");
            }
        }

    }

    /**
     * Method for case "TAMBAH_KARYAWAN".
     * Instantiate a Karyawan and add the Karyawan to a Korporasi.
     * @param type type of the Karyawan
     * @param name name of the Karyawan
     * @param salary salary of the Karyawan
     * @param korporasi the Korporasi that want to add the Karyawan
     * @return information about the case executed.
     */
    private static String addEmployee(String type, String name, double salary, Korporasi korporasi) {
        Karyawan employee = korporasi.findEmployee(name);
        String info;

        if (!korporasi.canAddEmployee()) {
            info = String.format("Karyawan di %s sudah mencapai batas maksimum", korporasi.getName());
        } else if (employee != null) {
            info = String.format("Karyawan dengan nama %s telah terdaftar", name);
        } else {
            boolean validInput = true;
            switch (type) {
                case "MANAGER":
                    employee = new Manager(name, salary);
                    break;
                case "INTERN":
                    employee = new Intern(name, salary);
                    break;
                case "STAFF":
                    employee = new Staff(name, salary);
                    break;
                default:
                    validInput = false;
            }

            if (validInput) {
                korporasi.addEmployee(employee);
                info = String.format("%s mulai bekerja sebagai %s di %s",
                        name, type, korporasi.getName());
                info += checkAndPromote(korporasi, employee);
            } else {
                info = "Input tidak valid";
            }

        }
        return info;
    }

    /**
     * Method for case "STATUS".
     * Check the Karyawan status.
     * @param name name of the employee
     * @param korporasi korporasi where the employee work.
     * @return information about the case executed.
     */
    private static String employeeStatus(String name, Korporasi korporasi) {
        Karyawan employee = korporasi.findEmployee(name);
        String info;
        if (employee == null) {
            info = "Karyawan tidak ditemukan";
        } else {
            info = employee.getStatus();
        }

        return info;
    }

    /**
     * Method for case "TAMBAH_BAWAHAN".
     * Add subordinate to a superior.
     * @param superiorName name of the superior
     * @param subordinateName name of the subordinate
     * @param korporasi Korporasi where the superior and subordinate work.
     * @return information about the case executed
     */
    public static String addSubordinate(String superiorName,
                                        String subordinateName, Korporasi korporasi) {

        Karyawan employee = korporasi.findEmployee(superiorName);
        Karyawan subordinate = korporasi.findEmployee(subordinateName);
        String info;
        if (employee == null || subordinate == null) {
            info = "Nama tidak berhasil ditemukan";
        } else {
            Superior superior = null;
            if (employee instanceof Superior) {
                superior = (Superior) employee;
            }
            if (superior == null || !superior.canRecruit(subordinate)) {
                info = "Anda tidak layak memiliki bawahan";
            } else if (superior.findSubordinate(subordinateName) != null) {
                info = String.format("Karyawan %s telah menjadi bawahan %s",
                        subordinateName, superiorName);
            } else {
                superior.addSubordinate(subordinate);
                info = String.format("Karyawan %s berhasil ditambahkan "
                        + "menjadi bawahan %s", subordinateName, superiorName);
            }
        }

        return info;
    }

    /**
     * Method for case "GAJIAN".
     * Give salary to all employees.
     * @param korporasi Korporasi that want to give salary.
     * @return information about the case executed.
     */
    private static String payday(Korporasi korporasi) {
        String info;
        if (korporasi.getEmployeeList().size() == 0) {
            info = String.format("%s tidak memiliki karyawan", korporasi.getName());
        } else {
            info = "Semua karyawan telah diberikan gaji";
            for (int i = 0; i < korporasi.getEmployeeList().size(); i++) {
                Karyawan employee = korporasi.getEmployeeList().get(i);
                korporasi.giveSalary(employee);
                info += checkAndUpdateSalary(korporasi, employee);
                info += checkAndPromote(korporasi, employee);
            }
        }

        return info;
    }

    /**
     * Method for check the availibility of update salary,
     * then update the salary if the condition is fulfilled.
     * @param korporasi the Korporasi that want to check and
     *                  update staff's salary.
     * @param employee the Karyawan that his/her salary want to
     *                 be updated.
     * @return information about the case executed.
     */
    private static String checkAndUpdateSalary(Korporasi korporasi, Karyawan employee) {
        String info = "";
        if (korporasi.canUpdateSalary(employee)) {
            String oldSalary = employee.getStrSalary();
            korporasi.updateSalary(employee);
            String newSalary = employee.getStrSalary();
            info = String.format("\n%s mengalami kenaikan gaji sebesar "
                    + "10%% dari %s menjadi %s", employee.getName(), oldSalary, newSalary);
        }
        return info;
    }

    /**
     * Method for check the availibility of promote staff,
     * then promote the staff if the condition is fulfilled.
     * @param korporasi the Korporasi that want to check and promote staff.
     * @param employee the Karyawan that want to promoted
     * @return information about the case executed.
     */
    private static String checkAndPromote(Korporasi korporasi, Karyawan employee) {
        String info = "";
        if (employee.getType().equals("Staff")
                && employee.getSalary() > Staff.getSalaryLimit()) {
            korporasi.promoteStaff((Staff) employee);
            info = String.format("\nSelamat, %s telah dipromosikan "
                    + "menjadi MANAGER", employee.getName());
        }
        return info;
    }
}