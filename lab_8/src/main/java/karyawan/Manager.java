package karyawan;

import java.util.ArrayList;

public class Manager extends Karyawan implements Superior {
    private static final int SUBORDINATE_LIMIT = 10;
    private static final String[] SUBORDINATE_TYPES = {"Staff", "Intern"};
    private ArrayList<Karyawan> subordinateList = new ArrayList<Karyawan>();

    /**
     * Constructor of Manager.
     * @param name name of the manager
     * @param salary initial salary of the manager
     */
    public Manager(String name, double salary) {
        super("Manager", name, salary);
    }

    /**
     * Method for inititate Manager using Staff object.
     * Can be used when promote a Staff to Manager.
     *
     * @param staff Staff that used to initiate the Manager object.
     */
    public Manager(Staff staff) {
        super("Manager", staff.getName(), staff.getSalary());
    }

    /**
     * Method for add subordinate.
     *
     * @param subordinate the subordinate employee that want
     *                    to be added.
     */
    public void addSubordinate(Karyawan subordinate) {
        subordinateList.add(subordinate);
    }

    /**
     * Method for remove subordinate.
     *
     * @param subordinate the subordinate employee that want
     *                    to be removed.
     */
    public void removeSubordinate(Karyawan subordinate) {
        subordinateList.remove(subordinate);
    }

    /**
     * Method for find subordinate.
     *
     * @param name name of the subordinate
     * @return the subordinate employee if found,
     *         null if not found.
     */
    public Karyawan findSubordinate(String name) {
        for (Karyawan subordinate : subordinateList) {
            if (subordinate.getName().equals(name)) {
                return subordinate;
            }
        }
        return null;
    }

    /**
     * Getter for subordinateList.
     *
     * @return the list of subordinate.
     */
    public ArrayList<Karyawan> getSubordinateList() {
        return subordinateList;
    }

    /**
     * Method for check the availibilty to add a subordinate.
     *
     * @param subordinate the employee that want to be checked.
     * @return true if available to add, false if not available.
     */
    public boolean canRecruit(Karyawan subordinate) {
        if (subordinateList.size() < SUBORDINATE_LIMIT) {
            for (String subordinateType : SUBORDINATE_TYPES) {
                if (subordinate.getType().equals(subordinateType)) {
                    return true;
                }
            }
        }

        return false;
    }
}
