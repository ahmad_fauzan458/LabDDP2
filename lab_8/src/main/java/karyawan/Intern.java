package karyawan;

public class Intern extends Karyawan {

    /**
     * Constructor of Intern.
     * @param name name of the intern
     * @param salary initial salary of the intern
     */
    public Intern(String name, double salary) {
        super("Intern", name, salary);
    }

}


