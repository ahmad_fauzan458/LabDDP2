package karyawan;

import java.util.ArrayList;

public class Staff extends Karyawan implements Superior {
    private static final int SUBORDINATE_LIMIT = 10;
    private static final String SUBORDINATE_TYPE = "Intern";
    private ArrayList<Karyawan> subordinateList = new ArrayList<>();
    private static int salaryLimit;

    /**
     * Constructor of Staff.
     * @param name name of the staff
     * @param salary initial salary of the staff
     */
    public Staff(String name, double salary) {
        super("Staff", name, salary);
    }

    /**
     * Method for add subordinate.
     *
     * @param subordinate the subordinate employee that want
     *                    to be added.
     */
    public void addSubordinate(Karyawan subordinate) {
        subordinateList.add(subordinate);
    }

    /**
     * Method for remove subordinate.
     *
     * @param subordinate the subordinate employee that want
     *                    to be removed.
     */
    public void removeSubordinate(Karyawan subordinate) {
        subordinateList.remove(subordinate);
    }

    /**
     * Method for find subordinate.
     *
     * @param name name of the subordinate
     * @return the subordinate employee if found,
     *         null if not found.
     */
    public Karyawan findSubordinate(String name) {
        for (Karyawan subordinate : subordinateList) {
            if (subordinate.getName().equals(name)) {
                return subordinate;
            }
        }
        return null;
    }

    /**
     * Getter for subordinateList.
     *
     * @return the list of subordinate.
     */
    public ArrayList<Karyawan> getSubordinateList() {
        return subordinateList;
    }

    /**
     * Method for check the availibilty to add a subordinate.
     *
     * @param subordinate the employee that want to be checked.
     * @return true if available to add, false if not available.
     */
    public boolean canRecruit(Karyawan subordinate) {
        return subordinate.getType().equals(SUBORDINATE_TYPE)
                && subordinateList.size() < SUBORDINATE_LIMIT;
    }

    /**
     * Getter method for the salary limit of Staff.
     * @return the salary limit of Staff
     */
    public static int getSalaryLimit() {
        return salaryLimit;
    }

    /**
     * Setter method for the salary limit of Staff.
     * @return the salary limit of Staff
     */
    public static void setSalaryLimit(int salaryLimit) {
        Staff.salaryLimit = salaryLimit;
    }
}
