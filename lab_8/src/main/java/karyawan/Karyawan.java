package karyawan;

public abstract class Karyawan {
    private String name;
    private String type;
    private double salary;
    private int receiveSalaryCount = 0;

    /**
     * Default constructor for any Karyawan.
     * @param type type of the Karyawan
     * @param name name of the Karyawan
     * @param salary initial salary of the Karyawan
     */
    public Karyawan(String type, String name, double salary) {
        this.type = type;
        this.name = name;
        this.salary = salary;
    }

    /**
     * Method getter for salary.
     *
     * @return salary of the Karyawan
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Method getter for string version of salary.
     * If double salary same as the int salary,
     * the string version will use the int.
     *
     * @return Str salary of the Karyawan
     */
    public String getStrSalary() {
        String salary;
        if (this.salary == (int) this.salary) {
            salary = Integer.toString((int) this.salary);
        } else {
            salary = Double.toString(this.salary);
        }
        return salary;
    }

    /**
     * Setter method for salary.
     *
     * @param salary salary of the Karyawan
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * Getter method for name.
     *
     * @return name of the Karyawan
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter method for type.
     *
     * @return type of the Karyawan
     */
    public String getType() {
        return this.type;
    }

    /**
     * Getter method for status.
     *
     * @return status of the Karyawan
     */
    public String getStatus() {

        return String.format("%s %s", this.name, this.getStrSalary());
    }

    /**
     * Method for adding count to receiveSalaryCount.
     * Add 1 to receiveSalaryCount anytime it's called
     */
    public void receiveSalary() {
        this.receiveSalaryCount += 1;
    }

    /**
     * Getter method for receiveSalaryCount.
     *
     * @return the number of receiveSalaryCount
     */
    public int getReceiveSalaryCount() {
        return receiveSalaryCount;
    }
}
