package karyawan;

import java.util.ArrayList;

/**
 * Interface for superior employee
 */
public interface Superior {

    void addSubordinate(Karyawan subordinate);

    void removeSubordinate(Karyawan subordinate);

    Karyawan findSubordinate(String name);

    ArrayList<Karyawan> getSubordinateList();

    boolean canRecruit(Karyawan subordinate);


}
