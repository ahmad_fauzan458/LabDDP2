import java.util.ArrayList;

import karyawan.Karyawan;
import karyawan.Manager;
import karyawan.Staff;

public class Korporasi {
    private static final int EMPLOYEE_LIMIT = 10000;
    private ArrayList<Karyawan> employeeList = new ArrayList<Karyawan>();
    private String name;

    /**
     * Constructor of Korporasi.
     * @param name name of the Korporasi
     */
    public Korporasi(String name) {
        this.name = name;
    }

    /**
     * Getter method for name.
     * @return name of the Korporasi
     */
    public String getName() {
        return this.name;
    }

    /**
     * check availibility to add employee.
     * @return true if can add employee, false if not
     */
    public boolean canAddEmployee() {
        return employeeList.size() < EMPLOYEE_LIMIT;
    }

    /**
     * Method for add employee.
     * @param employee Karyawan that want to be added.
     */
    public void addEmployee(Karyawan employee) {
        employeeList.add(employee);
    }

    /**
     * Method for remove employee.
     * @param employee Karyawan that want to be removed.
     */
    public void removeEmployee(Karyawan employee) {
        employeeList.remove(employee);
    }

    /**
     * Method for find employee.
     * @param name name of the employee.
     * @return the employee if found, null if not found.
     */
    public Karyawan findEmployee(String name) {
        for (Karyawan employee : employeeList) {
            if (employee.getName().equals(name)) {
                return employee;
            }
        }
        return null;
    }

    /**
     * Getter method for employee list.
     * @return list of employee.
     */
    public ArrayList<Karyawan> getEmployeeList() {
        return this.employeeList;
    }

    /**
     * Method for give salary to a Karyawan.
     * @param employee the Karyawan that want to be paid.
     */
    public void giveSalary(Karyawan employee) {
        employee.receiveSalary();
    }

    /**
     * Check the availibility to update salary.
     * @param employee the employee.
     * @return true if can update, false if not.
     */
    public boolean canUpdateSalary(Karyawan employee) {
        int count = employee.getReceiveSalaryCount();
        return count != 0 && count % 6 == 0;
    }

    /**
     * Method for update salary.
     * @param employee the employee that want to be updated
     */
    public void updateSalary(Karyawan employee) {
        double oldSalary = employee.getSalary();
        double newSalary = oldSalary + (0.1 * oldSalary);
        employee.setSalary(newSalary);
    }

    /**
     * Method for promote staff.
     * @param staff staff that want to be promoted.
     */
    public void promoteStaff(Staff staff) {
        Manager manager = new Manager(staff);
        for (Karyawan subordinate : staff.getSubordinateList()) {
            manager.addSubordinate(subordinate);
        }
        this.removeEmployee(staff);
        this.addEmployee(manager);
    }
}
